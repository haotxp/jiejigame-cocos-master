import MtlTopPanelView from './MtlTopPanelView';
import LogHelp from '../../../Log/LogHelp';
import { Player } from '../../../manager/UserMgr';
import GameMtlNetMgr from '../net/GameMtlNetMgr';

export default class MtlTopPanelController {
   
    public static Instance: MtlTopPanelController = null
    private view: MtlTopPanelView;

    public player: Player;

    constructor(v: MtlTopPanelView){
        MtlTopPanelController.Instance = this;
        this.view = v;

        this.view.btn_detail.node.on("click", this.onClickDetailBtn, this)
        this.view.btn_kaijiang.node.on("click", this.onClickKaijiangBtn, this)
        this.view.btn_goBackHall.node.on("click", this.onClickBackHallBtn, this)

        this.player = new Player()
    }

    private onClickDetailBtn(){
        LogHelp.log("onClickDetailBtn")
    }

    private onClickKaijiangBtn() {
        // LogHelp.log("onClickKaijiangBtn")
        GameMtlNetMgr.Instance.sendKaijiang()
    }

    private onClickBackHallBtn() {
        LogHelp.log("onClickBackHallBtn")
    }

    /**
     * 初始化玩家
     */
    public initPlayer(joinAry: string[]) {

        this.player.playerId = joinAry[1]
        this.player.nickName = joinAry[2]
        this.player.haveMoney = Number(joinAry[3])
        this.player.sex = Number(joinAry[4])
        this.player.headUrl = joinAry[5]

        this.updatePlayerView()
    }

    public updatePlayerView() {
        this.view.player_money_num.string = this.player.haveMoney.toFixed()
    }

    /**
     * 更新用户钱
     * @param arr 
     * 5,1000,0
     * 按钮的下标，筹码数,金钱类别（0:金币,1:金牛币,2:银牛币）
     */
    public updatePlayerMoney(arr: string[]) {
        if (arr[2] == "0") {
            let _tempMoney = Number(arr[1]);
            this.player.haveMoney -= _tempMoney

            this.updatePlayerView()
        }
    }

    /**
     * 更新奖池
     * @param msg 
     */
    public updatePool(msg: string[]) {
        // if (msg[1] == 1) {
        //     if (!isPlaying) {
        //         poolDH.play();
        //         isPlaying = true;
        //     }
        // } else {
        //     poolDH.stop();
        //     isPlaying = false;
        // }
        this.view.game_pool_num.string = msg[0]
    }

    public show() {
        this.view.Show()
    }

    public hide(){
        this.view.Hide()
    }
    
}
