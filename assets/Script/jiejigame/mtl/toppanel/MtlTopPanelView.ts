import { UIBase } from '../../../components/UIBase';
import MtlTopPanelController from './MtlTopPanelController';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MtlTopPanelView extends UIBase {

    /** 玩家说明 */
    public btn_detail: cc.Button;
    /** 开奖记录 */
    public btn_kaijiang: cc.Button;
    /** 返回大厅 */
    public btn_goBackHall: cc.Button;
    /** 奖池 */
    public game_pool_num: cc.Label;
    /** 玩家钱 */
    public player_money_num: cc.Label;

    private comp: MtlTopPanelController;
    
    public Awake(){
        this.Show()
        this.btn_detail = this.FindChildByName("btn_detail", cc.Button);
        this.btn_kaijiang = this.FindChildByName("btn_kaijiang", cc.Button);
        this.btn_goBackHall = this.FindChildByName("btn_goBackHall", cc.Button);

        this.game_pool_num = this.FindChildByName("game_pool_num", cc.Label);
        this.player_money_num = this.FindChildByName("player_money_num", cc.Label);

        this.resetData();

        this.comp = new MtlTopPanelController(this)
    }

    public resetData(){
        this.game_pool_num.string = "0";
        this.player_money_num.string = "0";
    }


}
