import MtlResultPanelView from './MtlResultPanelView';
import MtlGamePanelController from '../forestgamepanel/MtlGamePanelController';

export default class MtlResultPanelController {
    
    public static Instance: MtlResultPanelController = null;
    private view: MtlResultPanelView;

    public winColorIndex: number;
    public winAnimalIndex: number;

    constructor(v: MtlResultPanelView){
        MtlResultPanelController.Instance = this
        this.view = v
    }

    public initWinAnimalAndColor(arr: string[]) {
        this.winAnimalIndex = Number(arr[0])
        this.winColorIndex = Number(arr[1])
    }

    /**
     * 显示结算和动画
     * @param arr 
     */
    public showResult(msg: string[]) {
        this.view.Show()
        this.view.win_bet_num.string = msg[0]
        this.view.baocai_num.string = msg[2]

        // 显示获胜动物 颜色等

        // 显示大赢家
    }

    public clearAll(){
        this.view.Hide()
        MtlGamePanelController.Instance.getBackOriginalPosition()
    }
    
}
