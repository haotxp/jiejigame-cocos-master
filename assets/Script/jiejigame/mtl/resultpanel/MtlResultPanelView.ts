import MtlResultPanelController from './MtlResultPanelController';
import { UIBase } from '../../../components/UIBase';
import { Utils } from '../../../utils/Utils';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MtlResultPanelView extends UIBase {

    public an_bg: cc.Sprite;
    public an_an_colorbg: cc.Sprite;
    public bet_total_num: cc.Label;

    public win_bet_num: cc.Label;//自己盈利
    public winner_big_num: cc.Label;//本局大赢家
    public baocai_num: cc.Label;//暴彩

    private comp: MtlResultPanelController;
    public Awake(){
        this.Hide()
        this.an_bg = this.FindChildByName("an_bg", cc.Sprite)
        this.an_an_colorbg = Utils.FindComponentByName(this.an_bg.node, "an_an_colorbg", cc.Sprite)
        this.bet_total_num = Utils.FindComponentByName(this.an_bg.node, "bet_total_num", cc.Label)

        this.win_bet_num = this.FindChildByName("win_bet_num", cc.Label)
        this.winner_big_num = this.FindChildByName("winner_big_num", cc.Label)
        this.baocai_num = this.FindChildByName("baocai_num", cc.Label)

        this.resetData()
        this.comp = new MtlResultPanelController(this)
    }

    public resetData(){
        this.Hide()
        this.bet_total_num.string = "0"
        
        this.win_bet_num.string = "0"
        this.winner_big_num.string = "0"
        this.baocai_num.string = "0"
    }


}
