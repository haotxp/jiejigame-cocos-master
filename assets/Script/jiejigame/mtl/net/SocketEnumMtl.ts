
export default class SocketEnumMtl {
    //======================http============================

    //登陆获取token
    public static readonly MESSAGE_CONFIG_GET_SERVER: string = "gameLogin/signin";
    public static readonly MESSAGE_CONFIG_GET_SERVER_DEV: string = "gameLogin/loginDev";
    //游戏登陆
    public static readonly MESSAGE_GAME_LOGIN_DEV: string = "gameHall/onLoginDev";
    public static readonly MESSAGE_GAME_LOGIN_PROD: string = "gameHall/onLogin";
    //获取大厅配置文件
    public static readonly MESSAGE_GAME_HALL_CONFIG: string =
        "gameHall/getHallConfig";
    //获取服务器配置文件
    public static readonly MESSAGE_GAME_SERVER_CONFIG: string =
        "gameHall/getServerConfig";
    //获取用户余额
    public static readonly MESSAGE_GAME_PLAYER_BALANCE: string =
        "gameHall/getBalance";

    //==================================================

    //---------------------------------c to s 发出
    public static readonly CONN: number = 0;
    public static readonly BET: number = 1;
    public static readonly JOINDESK: number = 2;
    public static readonly CHAT: number = 8;
    public static readonly TREND: number = 12;
    public static readonly POTWIN: number = 13;

     //------------------------------------s to c 接收;

    public static readonly newTishiI: number = -1;//提示

    public static readonly initDeskI: number = 1;
    public static readonly joinDeskI: number = 2;//加入桌子

    public static readonly initAnimalAndColorI: number = 20;//初始化轮盘
    public static readonly initOddsAndTimesI: number = 21;//初始化倍数和时间
    public static readonly initBetAndPersonsI: number = 22;//初始化右边详细

    public static readonly winAnimalAndColorI: number = 23;//赢得动物和颜色

    public static readonly betMoneyI: number = 24;//是否下注成功
    public static readonly resultI: number = 25;//结算
    public static readonly gameOverI: number = 27;//整局游戏结束，开始下一局
    public static readonly chatI: number = 28;//接收聊天信息
    public static readonly trendI: number = 29;//开奖记录
    public static readonly poolI: number = 30;//彩池钱
    public static readonly potWinI: number = 31;//彩池中奖名单

    public static readonly startCompetI: number = 32;//接收开启挑战模式
    public static readonly competRankI: number = 33;//刷新排行消息
    public static readonly competResultI: number = 34;//自己的挑战成果
    public static readonly challengeI: number = 35;//挑战中途进入游戏

    public static readonly addOnlineI: number = 80;//显示在家玩家

    //====================提示框按钮类=================
    public static readonly  Tishi_tips: number = 0;
    public static readonly  Tishi_returnHall: number = 1;

    public static readonly getSupplyMoneyI: number = -10;//输光补助
    public static readonly updateLevelI: number = -11;//升级奖励
    
}
