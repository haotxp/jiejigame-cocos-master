import WebSocketManage from '../../../net/WebSocketManage';
import { GameSocketType, Define } from '../../../Define/Define';
import { Utils } from '../../../utils/Utils';
import { NotificationManage } from '../../../Notification/NotificationManage';
import { GameAudioMgr } from '../../../manager/GameAudioMgr';
import AlertPanelManager from '../../../manager/AlertPanelManager';
import SocketEnumMtl from './SocketEnumMtl';
import MtlGamePanelController from '../forestgamepanel/MtlGamePanelController';
import MtlTopPanelController from '../toppanel/MtlTopPanelController';
import MtlBetPanelController from '../betpanel/MtlBetPanelController';
import MtlTotalBetPanelController from '../totalbetpanel/MtlTotalBetPanelController';
import GameMtlTopUILayer from '../GameMtlTopUILayer';
import MtlResultPanelController from '../resultpanel/MtlResultPanelController';
import MtlWinRecordPanelController from '../winrecordpanel/MtlWinRecordPanelController';
import TipsManager from '../../../manager/TipsManager';
import GameMtlMainController from '../main/GameMtlMainController';
import LogHelp from '../../../Log/LogHelp';

const { ccclass, property } = cc._decorator;

/**
 * 摩天轮统一消息处理
 */
@ccclass
export default class GameMtlNetMgr extends cc.Component {

    private static __instance: GameMtlNetMgr;
    public static get Instance() {
        if (null == this.__instance) {
            this.__instance = new GameMtlNetMgr();
        }
        return this.__instance;
    }

    /** 自己的方位 */
    public directionDown: number = -1;
    /** 庄的方位 */
    public directionZhuang: number = -1;

    /**
     * 开始连接socket
     */
    public connecting() {
        // LogHelp.log("开始连接socket");
        WebSocketManage.Instance.CreateWebSocket(GameSocketType.SocketType_MTL,
            Define.MTL__WEB__SOCKET__URL,
            this.onConnectedSuccessCB.bind(this));
    }

    /**
     * 关闭游戏连接
     */
    public closeConnected() {
        WebSocketManage.Instance.CloseWebSocker(GameSocketType.SocketType_MTL);
    }

    /**
     * 连接成功
     * @param key 
     */
    public onConnectedSuccessCB() {
        // LogHelp.log("连接成功");
        this.joinDesk()
    }

    public joinDesk() {
        if (WebSocketManage.Instance.isConnecting(GameSocketType.SocketType_MTL) == -1) {
            this.connecting()
            return;
        }
        if (WebSocketManage.Instance.isConnecting(GameSocketType.SocketType_MTL) == 0) {
            WebSocketManage.Instance.ReconnectWebSocker(GameSocketType.SocketType_MTL);
            return;
        }
        let json = {
            "playerId": GameMtlMainController.Instance.currentPlayer.playerId,
            "token": Utils.GetLocalStorage(Define.STORAGE_TOKEN)
        }
        // 连接成功后，发送进入桌子请求
        this.SendMessage(SocketEnumMtl.CONN, JSON.stringify(json));
    }

    /**
     * 统一发送消息
     * 
     * @param command 
     * @param data 
     */
    public SendMessage(command: number, data: string = "") {
        // LogHelp.log(command + '，data: ' + data);
        data = command + "@" + data;

        WebSocketManage.Instance.SendMessage(GameSocketType.SocketType_MTL, command, data);
    }

    /**
     * 开奖记录
     */
    public sendKaijiang(){
        this.SendMessage(SocketEnumMtl.TREND);
    }

    /**
     * 查看大奖
     * @param content 
     */
    public sendPotWin(content: string) {
        this.SendMessage(SocketEnumMtl.POTWIN, content);
    }

    public sendBet(content: string) {
        this.SendMessage(SocketEnumMtl.BET, content);
    }

    public sendChat(chatInfo: string): void {
        this.SendMessage(SocketEnumMtl.CHAT, chatInfo);
    }

    //==========================接收消息处理=============

    /**
     * 注册消息
     */
    public AddRegisterMessage() {

        // MtlBetPanelController.Instance.updateBtnStatus(true)

        // 初始化桌子
        NotificationManage.Register(SocketEnumMtl.initDeskI,
            this.initDeskS.bind(this), this, GameSocketType.SocketType_MTL);

        // 加入桌子
        NotificationManage.Register(SocketEnumMtl.joinDeskI,
            this.joinDeskS.bind(this), this, GameSocketType.SocketType_MTL);

        // 初始化轮盘
        NotificationManage.Register(SocketEnumMtl.initAnimalAndColorI,
            this.initAnimalAndColorI.bind(this), this, GameSocketType.SocketType_MTL);

        // 初始化倍数和时间
        NotificationManage.Register(SocketEnumMtl.initOddsAndTimesI,
            this.initOddsAndTimesI.bind(this), this, GameSocketType.SocketType_MTL);

        // 初始化右边详细
        NotificationManage.Register(SocketEnumMtl.initBetAndPersonsI,
            this.initBetAndPersonsI.bind(this), this, GameSocketType.SocketType_MTL);

        // 赢得动物和颜色
        NotificationManage.Register(SocketEnumMtl.winAnimalAndColorI,
            this.winAnimalAndColorI.bind(this), this, GameSocketType.SocketType_MTL);

        // 是否下注成功
        NotificationManage.Register(SocketEnumMtl.betMoneyI,
            this.onBetMoneyI.bind(this), this, GameSocketType.SocketType_MTL);

        // 结算
        NotificationManage.Register(SocketEnumMtl.resultI,
            this.onResultI.bind(this), this, GameSocketType.SocketType_MTL);

        // 游戏结束
        NotificationManage.Register(SocketEnumMtl.gameOverI,
            this.onGameOver.bind(this), this, GameSocketType.SocketType_MTL);

        // 接收聊天信息
        NotificationManage.Register(SocketEnumMtl.chatI,
            this.onChatI.bind(this), this, GameSocketType.SocketType_MTL);

        // 开奖记录
        NotificationManage.Register(SocketEnumMtl.trendI,
            this.onTrendI.bind(this), this, GameSocketType.SocketType_MTL);

        // 彩池钱
        NotificationManage.Register(SocketEnumMtl.poolI,
            this.onPoolI.bind(this), this, GameSocketType.SocketType_MTL);

        // 彩池中奖名单
        NotificationManage.Register(SocketEnumMtl.potWinI,
            this.onPotWinI.bind(this), this, GameSocketType.SocketType_MTL);

        // 超时等待提示
        NotificationManage.Register(SocketEnumMtl.newTishiI,
            this.onTishi.bind(this), this, GameSocketType.SocketType_MTL);

    }

    /**
     * 初始化桌子
     * 
     * @param command 
     * @param data 
     */
    private initDeskS(command: string, data: string) {
        // let realSmg = data.split('@');
        // if (realSmg.length > 0) {
        //     let dir: number = Number(realSmg[1]); // 自己的方位
        //     this.directionDown = dir;
        // }
    }

    /**
     * 加入桌子
     * -1,1000000060,一米太阳吖,35378631,1,http://47.93.230.134:9001/mahjong/headIcon/boy/2474.png,2,24,11
     * 不用管，ID，昵称，金币，性别(0女，1男)，头像地址，游戏状态，牛币，小牛币
     */
    private joinDeskS(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            //GameAudioMgr.Instance.PlaySoundCommon("joinDesk.mp3"); //暂无资源，先关闭
            let joinAry: Array<string> = realSmg[1].split(','); // 显示玩家个人信息
            MtlTopPanelController.Instance.initPlayer(joinAry)
            MtlBetPanelController.Instance.showBet()

            if (joinAry[6] == "2") {
                GameMtlTopUILayer.Instance.showMask()
            }
        }
    }

    /** 初始化轮盘 */
    private initAnimalAndColorI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let aniAndColor: Array<string> = realSmg[1].split(';');
            MtlGamePanelController.Instance.setAnimalAndColor(aniAndColor)
        }
    }

    /**
     * 初始化倍数和时间
     * 31,13,17,6,20,9,11,4,17,7,9,3,;90
     * 倍数(按动物);时间
     */
    private initOddsAndTimesI(command: string, data: string) {

        cc.log("打印数据1", command, data)

        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let oddAndTimes: Array<string> = realSmg[1].split(';');
            let odd: string[] = oddAndTimes[0].split(",");//倍数
            cc.log("打印数据2", odd)
            MtlTopPanelController.Instance.show()
            cc.log("打印长度", odd.length)
            MtlBetPanelController.Instance.initOdd(odd)
            
            MtlGamePanelController.Instance.initTime(oddAndTimes[1])
            MtlTopPanelController.Instance.updatePlayerView()

            MtlTotalBetPanelController.Instance.clearAll()
            MtlTotalBetPanelController.Instance.show()
        }
    }

    /**
     * 初始化右边详细
     * 0,0,0,0,0,0,0,0,0,0,0,0,;0.0;1
     * 每个动物对应颜色的总注数;总下注;人数
     */
    private initBetAndPersonsI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: Array<string> = realSmg[1].split(';');
            MtlTotalBetPanelController.Instance.updateColorMoney(arr)
        }
    }

    /**
     * 赢得动物和颜色
     * 15,19
     * 动物，颜色
     */
    private winAnimalAndColorI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: string[] = realSmg[1].split(',');

            MtlTopPanelController.Instance.hide()
            MtlBetPanelController.Instance.hide()
            MtlTotalBetPanelController.Instance.hide()

            MtlResultPanelController.Instance.initWinAnimalAndColor(arr)
            MtlGamePanelController.Instance.playMtl(arr)
        }
    }

    /**
     * 下注成功更新元宝、牛币、小牛币
     * 5,1000,0
     * 按钮的下标，筹码数,金钱类别（0:金币,1:金牛币,2:银牛币）
     */
    private onBetMoneyI(command: string, data: string) {
        cc.log("下注成功消息回调", command, data)
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: string[] = realSmg[1].split(',');
            MtlTopPanelController.Instance.updatePlayerMoney(arr)

            //添加金额显示面板，并显示金额
            MtlBetPanelController.Instance.updateBetMoney(arr)
        }
    }

    /**
     * 本场游戏结束
     * 2000013515,2150
     * 玩家id,玩家金钱
     */
    private onResultI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: string[] = realSmg[1].split('#');

            MtlResultPanelController.Instance.showResult(arr)
            MtlTopPanelController.Instance.updatePlayerView()
            MtlBetPanelController.Instance.showBet()
        }
    }

    /**
     * 整局游戏结束，开始下一局
     */
    private onChatI(command: string, data: string) {
        // ChatControl.instance.showChat(msg);
    }

    /**
     * 开奖记录
     * 159,2000013515,孩子ta爹,7,70000,2,2,0;
     * 局数，ID，名字，倍数，获奖金额，动物，颜色，爆彩金额
     */
    private onTrendI(command: string, data: string) {
        cc.log("开奖记录", command,data)
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            if (realSmg[1] != null && realSmg[1] != "") {
                let msg: string[] = realSmg[1].split(';');
                MtlWinRecordPanelController.Instance.showTrend(msg)
            }
        }

    }

    /**
     * 更新彩池的钱
     */
    private onPoolI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            if (realSmg[1] != null && realSmg[1] != "") {
                let msg: string[] = realSmg[1].split('#');
                MtlTopPanelController.Instance.updatePool(msg)
            }
        }
    }

    /**
     * 彩池中奖名单
     * 1#10:30,封心锁爱,100000,34964168;10:30,戀人未满,100000,27617069;
     */
    private onPotWinI(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            if (realSmg[1] != null && realSmg[1] != "") {
                let msg: string[] = realSmg[1].split('#');
                
            }
        }
        // var ary: Array = msg.split("#");
        // PotWinControl.instance.potMsg = ary[1].toString().split(";");
        // PotWinControl.instance.curPage = 1;
        // PotWinControl.instance.nowState = ary[0];
        // PotWinControl.instance.showPotWin();
    }

    /**
     * 游戏结束
     * 
     * @param command 
     * @param data 
     */
    private onGameOver(command: string, data: string) {
        MtlResultPanelController.Instance.clearAll()
        MtlGamePanelController.Instance.clearAll()
    }

    /**
     * 超时等待提示 错误提示
     * 
     * @param command 
     * @param data 
     */
    private onTishi(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: Array<string> = realSmg[1].split(';');

            var typeArr = arr[2].split(",");
            for (let i = 0; i < typeArr.length; i++) {
                switch (Number(typeArr[i])) {
                    case SocketEnumMtl.Tishi_returnHall:
                        {
                            AlertPanelManager.Instance.show(arr[1], function () {
                                // HoolaiForestDanceControl.instance.backHallCB
                            }, function () {
                                // HoolaiForestDanceControl.instance.backHallCB
                            });
                            break;
                        }
                    case SocketEnumMtl.Tishi_tips:
                        {
                            TipsManager.Instance.createTips(arr[1]);
                            break;
                        }
                }
            }
        }
    }

    //========================================


}
