import MtlGamePanelView from './MtlGamePanelView';
import ScheduleMgr from '../../../utils/ScheduleMgr';
import MtlBetPanelController from '../betpanel/MtlBetPanelController';

/**
 * 转盘
 */
export default class MtlGamePanelController {
    public static Instance: MtlGamePanelController = null
    private view: MtlGamePanelView

    private currentTime: number = 0;

    private winColorIndex: number;
    private winAnimalIndex: number;

    private oriPos :cc.Vec2;

    constructor(v: MtlGamePanelView){
        MtlGamePanelController.Instance = this
        this.view = v
        this.oriPos = this.view.node.position
    }

    /**
     * 设置轮盘
     * 1,2,3,4 青蛙，熊猫，猴子，兔子；0，1，2，3 红，绿，黄，无
     */
    public setAnimalAndColor(msg: Array<string>) {
        let animal: string[] = msg[0].split(",");
        let color: string[] = msg[1].split(",");

        //初始化轮盘动物
        for (let i = 0; i < animal.length; i++) {
            if (animal[i] == null || animal[i] == "") return;
            // animalAry[i].gotoAndStop(animal[i]);
        }

        //初始化轮盘颜色
        for (let i = 0; i < color.length; i++) {
            if (color[i] == null || color[i] == "") return;
            // colorAry[i].gotoAndStop(int(color[i]) + 1);
        }
    }

    /**
     * 初始化时间
     */
    public initTime(time: string) {
        this.clearAll()
        this.view.game_time_num.node.active = true;
        this.currentTime = Number(time);
        this.view.game_time_num.string = time

        ScheduleMgr.Instance.schedule(this.onUpdateTime, this, 1, this.currentTime)
        //修改测试
        // cc.log("执行顺序3")
        // this.view.schedule(this.onUpdateTime, 1, this.currentTime)
    }

    private onUpdateTime() {
        cc.log("执行顺序2")
        if (this.currentTime <= 0) {
            this.unDirSchedule();
            MtlBetPanelController.Instance.updateBtnStatus(true)
            return;
        }
        this.currentTime--;
        this.view.game_time_num.string = this.currentTime.toString()
    }

    private unDirSchedule() {
        ScheduleMgr.Instance.unschedule(this.onUpdateTime, this)

        //修改测试
        // cc.log("执行顺序1")
        // this.view.unschedule(this.onUpdateTime)
    }

    //============================================

    /**
     * 开始移动摩天轮
     */
    public playMtl(arr: string[]) {
        this.HideCountDown()

        this.winAnimalIndex = Number(arr[0])
        this.winColorIndex = Number(arr[1])
        
        this.view.node.runAction(cc.scaleTo(3, 1, 1).easing(cc.easeCubicActionOut()));
        this.view.node.runAction(cc.sequence(
            cc.moveTo(3, cc.v2(0, 0)).easing(cc.easeCubicActionOut()),
            cc.callFunc(this.PlayForest, this, true)
        ));
    }

    /* 
    *摩天轮回到初始位置
    */
    public getBackOriginalPosition(){
        this.view.node.runAction(cc.scaleTo(3, 1, 1).easing(cc.easeCubicActionOut()));
        this.view.node.runAction(cc.moveTo(3, this.oriPos).easing(cc.easeCubicActionOut()));
    }

    /**
     * 播放转圈圈动画
     * @param msg 
     */
    public PlayForest() {
        let animalRotation: number = (360 * 10 + (20 - this.winAnimalIndex + this.winColorIndex) * 18) - this.view.ans.angle;
        let pointerRotation: number = -360 * 10 + this.view.colorList[this.winColorIndex].node.angle - this.view.game_pointer.node.angle;

        this.view.ans.runAction(
            cc.sequence(
                cc.rotateBy(15, animalRotation).easing(cc.easeCircleActionOut()),
                cc.callFunc(this.ShowWin, this, true)));

        this.view.game_pointer.node.runAction(cc.rotateBy(9.5, pointerRotation).easing(cc.easeCubicActionOut()));
    }

    /**
     * 显示胜利
     */
    public ShowWin() {
        // this.view.animalList[this.winAnimalIndex].winAnimation.node.active = true;
        // this.view.colorList[this.winColorIndex].colorWinAnimation.node.active = true;
        // this.view.animalList[this.winAnimalIndex].winAnimation.play();
        // this.view.colorList[this.winColorIndex].colorWinAnimation.play();

        // /**
        //  * 将动物转盘和指针回归到360度以内
        //  */
        // this.view.animalNode.angle = this.view.animalNode.angle - 360 * 10;
        // this.view.pointer.node.angle = this.view.pointer.node.angle + 360 * 10;
    }

    /**
     * 隐藏倒计时
     */
    public HideCountDown() {
        this.unDirSchedule()
        this.view.game_time_num.node.active = false;
        this.currentTime = 0
    }

    public clearAll(){
        this.view.resetData()
        this.unDirSchedule()
    }

}
