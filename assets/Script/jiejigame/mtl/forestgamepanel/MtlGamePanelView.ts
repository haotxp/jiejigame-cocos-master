import { UIBase } from '../../../components/UIBase';
import MtlGamePanelController from './MtlGamePanelController';
import { Utils } from '../../../utils/Utils';
const {ccclass, property} = cc._decorator;

/**
 * 转盘
 */
@ccclass
export default class MtlGamePanelView extends UIBase {

    public color: cc.Node;
    public ans: cc.Node;

    public pointBg: cc.Sprite;
    public game_pointer: cc.Sprite;
    public game_time_num: cc.Label;

    public colorList: cc.Sprite[] = []
    public ansList: cc.Sprite[] = []

    private comp: MtlGamePanelController;
    public Awake(){
        this.color = this.FindNodeByName("color")
        this.ans = this.FindNodeByName("ans")

        let arr = this.color.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("c") != -1) {
                    this.colorList.push(value)
                }
            })
        }

        arr = this.ans.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an") != -1) {
                    this.ansList.push(value)
                }
            })
        }

        this.pointBg = this.FindChildByName("pointBg", cc.Sprite)
        this.game_pointer = this.FindChildByName("game_pointer", cc.Sprite)
        this.game_time_num = this.FindChildByName("game_time_num", cc.Label)

        this.resetData()
        this.comp = new MtlGamePanelController(this)
    }

    public resetData(){
        this.game_time_num.string = "00"

        this.color.angle = 0
        this.ans.angle = 0
    }
}
