import { UIBase } from '../../components/UIBase';
const {ccclass, property} = cc._decorator;
@ccclass
export default class GameMtlTopUILayer extends UIBase {

    public static Instance: GameMtlTopUILayer
    public msg: cc.Label
    public bg: cc.Sprite
    private currentPanel: cc.Node

    public Awake(){
        this.Hide()
        this.msg = this.FindChildByName("msg", cc.Label)
        this.bg = this.FindChildByName("bg", cc.Sprite)

        this.msg.node.active = false
        GameMtlTopUILayer.Instance = this
    }

    public onShowPanel(c: cc.Node){
        if (this.node.isChildOf(c)) return

        this.msg.node.active = false
        this.currentPanel = c
        this.Show()
        this.node.addChild(c)
    }

    public Hide(){
        super.Hide()
        this.node.active = false
        this.node.removeChild(this.currentPanel)
    }

    /**
     * 显示mask
     */
    public showMask(){
        this.msg.node.active = true
        this.Show()
    }


}
