import MtlWinRecordPanelView from './MtlWinRecordPanelView';
import { GamePoolMgr } from '../../../manager/GamePoolMgr';
import { GamePoolsName } from '../../../Define/Define';
import { GameLoadResourceMgr } from '../../../Load/GameLoadResourceMgr';
import { Utils } from '../../../utils/Utils';
import MtlRecordItem from './MtlRecordItem';

export default class MtlWinRecordPanelController {
    
    public static Instance: MtlWinRecordPanelController = null
    private view: MtlWinRecordPanelView

    private recordList: cc.Node[] = []

    constructor(v: MtlWinRecordPanelView){
        MtlWinRecordPanelController.Instance = this
        this.view = v
    }

    /**
     * 显示开奖记录
     * 局数，ID，名字，倍数，获奖金额，动物，颜色，爆彩金额
     * @param msg 
     */
    public showTrend(msg: string[]) {
        this.clearAll()
        this.recordList = []
        if(msg != null && msg.length > 0){
            for (let i = 0; i < msg.length; i++) {
                if (msg[i] == null || msg[i] == "") break;
                var trendPanelAry = msg[i].toString().split(",");

                let node: cc.Node = GamePoolMgr.Instance.GetNode(GamePoolsName.POOL_MTL_RECORD_ITEM)
                let ri: MtlRecordItem
                if (node == null) {
                    let profab = GameLoadResourceMgr.Instance.GetTempLoadResourceControl().GetPrefabByUrl("record_item")
                    node = Utils.PrefabInstantiate(profab)
                    ri = node.addComponent(MtlRecordItem)
                    ri.Awake()
                }else{
                    ri = node.addComponent(MtlRecordItem)
                    ri.resetData()
                }

                ri.initData(trendPanelAry)
                this.recordList.push(node)
                this.view.recordList.content.addChild(node)
            }
        }
    }

    public clearAll(){
        this.view.Hide()
        while (this.recordList.length > 0){
            GamePoolMgr.Instance.PutNode(GamePoolsName.POOL_MTL_RECORD_ITEM, this.recordList.shift())
        }
        this.recordList = []
    }
    
}
