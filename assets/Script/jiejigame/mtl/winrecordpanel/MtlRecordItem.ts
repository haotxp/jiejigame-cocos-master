import { UIBase } from '../../../components/UIBase';
import { Utils } from '../../../utils/Utils';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MtlRecordItem extends UIBase {

    public room_id: cc.Label
    /** 动物 */
    public an_color_bg: cc.Sprite
    /** 颜色 */
    public color_bg: cc.Sprite

    public player_name: cc.Label
    /** 倍数 */
    public player_odd: cc.Label
    /** 赢的筹码数量 */
    public player_win_bet: cc.Label

    public Awake(){
        this.room_id = this.FindChildByName("room_id", cc.Label)

        this.an_color_bg = this.FindChildByName("an_color_bg", cc.Sprite)
        this.color_bg = Utils.FindComponentByName(this.an_color_bg.node, "color_bg", cc.Sprite)

        this.player_name = this.FindChildByName("player_name", cc.Label)
        this.player_odd = this.FindChildByName("player_odd", cc.Label)
        this.player_win_bet = this.FindChildByName("player_win_bet", cc.Label)

        this.resetData()
    }

    resetData() {
        this.room_id.string = ""
        this.player_name.string = ""
        this.player_odd.string = ""
        this.player_win_bet.string = ""

        this.an_color_bg.spriteFrame = null
        this.color_bg.spriteFrame = null
    }

    initData(trendPanelAry: string[]){
        // var trendPanel: TrendPanelView = new TrendPanelView();
        this.room_id.string = trendPanelAry[0];
        this.player_name.string = trendPanelAry[2];
        this.player_odd.string = trendPanelAry[3];
        this.player_win_bet.string = trendPanelAry[4];

        // trendPanel.animal.index = trendPanelAry[5];
        // trendPanel.color.index = trendPanelAry[6];

    }

    Hide(){
        super.Hide()
    }
    
}
