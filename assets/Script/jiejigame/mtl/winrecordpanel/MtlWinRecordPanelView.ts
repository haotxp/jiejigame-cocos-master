import { UIBase } from '../../../components/UIBase';
import MtlWinRecordPanelController from './MtlWinRecordPanelController';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MtlWinRecordPanelView extends UIBase {

    public btn_close: cc.Button;
    public recordList: cc.ScrollView;

    private comp: MtlWinRecordPanelController;
    public Awake(){
        this.Hide()
        this.btn_close = this.FindChildByName("btn_close", cc.Button)
        this.recordList = this.FindChildByName("recordList", cc.ScrollView)

        this.comp = new MtlWinRecordPanelController(this)
    }

    public resetData(){

    }
    
}
