import { UIBase } from '../../../components/UIBase';
import GameMtlAnimationLayerController from './GameMtlAnimationLayerController';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMtlAnimationLayerView extends UIBase {

    public game_effect_center: sp.Skeleton;
    public game_effect_up: sp.Skeleton;
    public game_effect_down: sp.Skeleton;

    private comp: GameMtlAnimationLayerController

    public Awake(){
        this.game_effect_center = this.FindChildByName("game_effect_center", sp.Skeleton);
        this.game_effect_up = this.FindChildByName("game_effect_up", sp.Skeleton);
        this.game_effect_down = this.FindChildByName("game_effect_down", sp.Skeleton);

        this.game_effect_center.node.active = false
        this.game_effect_up.node.active = false
        this.game_effect_down.node.active = false

        this.comp = new GameMtlAnimationLayerController(this)
    }

    
}
