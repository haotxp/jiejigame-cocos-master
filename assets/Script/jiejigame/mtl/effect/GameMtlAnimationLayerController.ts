import { PlayerDirectionType, GameAnimationNames } from '../../../Define/Define';
import { GameLoadResourceMgr } from '../../../Load/GameLoadResourceMgr';
import GameMtlAnimationLayerView from './GameMtlAnimationLayerView';

export default class GameMtlAnimationLayerController {

    public static Instance: GameMtlAnimationLayerController = null

    public view: GameMtlAnimationLayerView

    public constructor(v: GameMtlAnimationLayerView){
        GameMtlAnimationLayerController.Instance = this
        this.view = v
    }

    public showAnimation(dir: PlayerDirectionType, name: GameAnimationNames){
        let sd = GameLoadResourceMgr.Instance.GetTempLoadResourceControl().getSkeletonDataRes(name)
        if (sd == null){
            return
        }
        let currentSk: sp.Skeleton
        switch (dir) {
            case PlayerDirectionType.CENTER:
                currentSk = this.view.game_effect_center
                break;
            case PlayerDirectionType.DOWN:
                currentSk = this.view.game_effect_down
                break;
            case PlayerDirectionType.RIGHT:

                break;
            case PlayerDirectionType.UP:
                currentSk = this.view.game_effect_up
                break;
            case PlayerDirectionType.LEFT:

                break;
        }

        if (currentSk != null) {
            currentSk.node.active = true
            currentSk.skeletonData = sd
            currentSk.setAnimation(0, name, false)

            currentSk.setCompleteListener(() => {
                currentSk.node.active = false
                currentSk.skeletonData = null
            })
            
        }
    }
    
}
