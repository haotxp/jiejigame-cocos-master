import { UIBase } from '../../../components/UIBase';
import { Utils } from '../../../utils/Utils';
import MtlBetPanelController from './MtlBetPanelController';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MtlBetPanelView extends UIBase {

    public title: cc.Button;
    public title_toggle: cc.Toggle;

    // public betNumChoose: cc.ToggleContainer;
    public bet_num_toggle1: cc.Toggle;
    public bet_num_toggle2: cc.Toggle;
    public bet_num_toggle3: cc.Toggle;
    public betNum: cc.Label[];

    public betAns: cc.Node;
    public redList: cc.Sprite[];
    public greenList: cc.Sprite[];
    public yellowList: cc.Sprite[];
    public ansList: cc.Sprite[];//按照动物和颜色排序

    public oldPos: cc.Vec2;

    public btnNameAry: string[] = ["btn_1_0", "btn_1_1", "btn_1_2",
        "btn_2_0", "btn_2_1", "btn_2_2",
        "btn_3_0", "btn_3_1", "btn_3_2",
        "btn_4_0", "btn_4_1", "btn_4_2"];

    private comp: MtlBetPanelController;

    public Awake() {
        this.Show()
        this.oldPos = this.node.position

        this.redList = []
        this.greenList = []
        this.yellowList = []
        this.betNum = []
        this.ansList = []

        this.title = this.FindChildByName("title", cc.Button)
        this.title_toggle = this.FindChildByName("title_toggle", cc.Toggle)
        this.title_toggle = this.FindChildByName("title_toggle", cc.Toggle)

        // this.betNumChoose = this.FindChildByName("betNumChoose", cc.ToggleContainer)
        this.bet_num_toggle1 = this.FindChildByName("bet_num_toggle1", cc.Toggle)
        this.bet_num_toggle2 = this.FindChildByName("bet_num_toggle2", cc.Toggle)
        this.bet_num_toggle3 = this.FindChildByName("bet_num_toggle3", cc.Toggle)
        this.betNum.push(Utils.FindComponentByName(this.bet_num_toggle1.node, "bet_num", cc.Label));
        this.betNum.push(Utils.FindComponentByName(this.bet_num_toggle2.node, "bet_num", cc.Label));
        this.betNum.push(Utils.FindComponentByName(this.bet_num_toggle3.node, "bet_num", cc.Label));

        this.comp = new MtlBetPanelController(this)

        this.betAns = this.FindNodeByName("betAns")
        let red: cc.Node = Utils.FindChildNodeByName(this.betAns, "red")
        let arr = red.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    value.node.on('click', this.comp.onClickRedBtn, this.comp);
                    this.redList.push(value)
                }
            })

            this.redList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        let green: cc.Node = Utils.FindChildNodeByName(this.betAns, "green")
        arr = green.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    value.node.on('click', this.comp.onClickGreenBtn, this.comp);
                    this.greenList.push(value)
                }
            })

            this.greenList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        let yellow: cc.Node = Utils.FindChildNodeByName(this.betAns, "yellow")
        arr = yellow.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    value.node.on('click', this.comp.onClickYellowBtn, this.comp);
                    this.yellowList.push(value)
                }
            })

            this.yellowList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        for (let index = 0; index < 4; index++) {
            this.ansList.push(this.redList[index]);
            this.ansList.push(this.greenList[index]);
            this.ansList.push(this.yellowList[index]);
        }

        for (let index = 0; index < this.ansList.length; index++) {
            this.ansList[index].node.name = this.btnNameAry[index]
        }

        this.resetData()
    }

    public resetData() {
        this.title_toggle.isChecked = true;
        let bg = Utils.FindComponentByName(this.title_toggle.node, "Background", cc.Sprite);
        bg.node.active = false

        this.bet_num_toggle1.isChecked = true;
        this.bet_num_toggle2.isChecked = false;
        this.bet_num_toggle3.isChecked = false;

        if (this.ansList != null && this.ansList.length > 0) {
            this.ansList.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("btn_") != -1) {
                    let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
                    btn.string = "0";

                    let amn = Utils.FindComponentByName(value.node, "an_multiple_num", cc.Label);
                    amn.string = "0";
                }
            })
        }

        // if (this.greenList != null && this.greenList.length > 0) {
        //     this.greenList.forEach((value: cc.Sprite, index: number) => {
        //         if (value.node.name.indexOf("an_bg") != -1) {
        //             let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
        //             btn.string = "0";

        //             let amn = Utils.FindComponentByName(value.node, "an_multiple_num", cc.Label);
        //             amn.string = "0";
        //         }
        //     })
        // }

        // if (this.yellowList != null && this.yellowList.length > 0) {
        //     this.yellowList.forEach((value: cc.Sprite, index: number) => {
        //         if (value.node.name.indexOf("an_bg") != -1) {
        //             let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
        //             btn.string = "0";

        //             let amn = Utils.FindComponentByName(value.node, "an_multiple_num", cc.Label);
        //             amn.string = "0";
        //         }
        //     })
        // }
    }

}
