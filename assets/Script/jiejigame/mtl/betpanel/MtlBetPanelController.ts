import MtlBetPanelView from './MtlBetPanelView';
import TweenMgr from '../../../utils/TweenMgr';
import LogHelp from '../../../Log/LogHelp';
import { Utils } from '../../../utils/Utils';
import MtlTopPanelController from '../toppanel/MtlTopPanelController';
import GameMtlNetMgr from '../net/GameMtlNetMgr';
import TipsManager from '../../../manager/TipsManager';

export default class MtlBetPanelController {
    
    public static Instance: MtlBetPanelController = null;
    private view: MtlBetPanelView;

    private currentBetNum: number = 0;
    private currentIndex: number = 0;

    /**
     *设置玩家下注筹码金额
    */
    public arrChips: string[][] = [
        ["0.01", "0.02", "0.05", "0.1", "1"],
        ["0.02", "0.1", "0.5", "1", "5"],
        ["0.1", "0.5", "1", "2", "5"],
        ["0.5", "1", "2", "5", "10"],
        ["1", "10", "50", "100", "500"]];

    private beishuAry: string[] = ["1_0", "1_1", "1_2",
        "2_0", "2_1", "2_2",
        "3_0", "3_1", "3_2",
        "4_0", "4_1", "4_2"];

    constructor(v: MtlBetPanelView){
        MtlBetPanelController.Instance = this
        this.view = v

        this.view.title.node.on('click', this.onClickTitleCB, this);
        this.view.title_toggle.node.on('toggle', this.onClickTitleToggleCB, this);

        this.view.bet_num_toggle1.node.on('toggle', this.onClickBetNumCB, this);
        this.view.bet_num_toggle2.node.on('toggle', this.onClickBetNumCB, this);
        this.view.bet_num_toggle3.node.on('toggle', this.onClickBetNumCB, this);

        this.currentIndex = 0;
        this.setChips(0)
        this.currentBetNum = Number(this.view.betNum[0].string);
        // LogHelp.log("currentIndex: " + this.currentIndex)
        // LogHelp.log("currentBetNum: " + this.currentBetNum)
    }

    private onClickTitleCB() {
        if (this.view.title_toggle.isChecked == false) {
            this.view.title_toggle.isChecked = true
        }else{
            this.view.title_toggle.isChecked = false
        }
    }

    private onClickTitleToggleCB(toggle: cc.Toggle) {
        let op = this.view.oldPos
        if (toggle.isChecked == false) {
            let bg = Utils.FindComponentByName(toggle.node, "Background", cc.Sprite);
            bg.node.active = true

            TweenMgr.moveTo(this.view.node, 0.5, op.x, (op.y - this.view.node.height + 50));
        } else {
            let bg = Utils.FindComponentByName(toggle.node, "Background", cc.Sprite);
            bg.node.active = false

            TweenMgr.moveTo(this.view.node, 0.5, op.x, op.y);
        }
    }

    private onClickBetNumCB(toggle: cc.Toggle) {
        this.currentIndex = Number(toggle.node.name.substring(14));
        this.currentBetNum = Number(this.view.betNum[this.currentIndex - 1].string);
        // LogHelp.log("currentIndex: " + this.currentIndex)
        // LogHelp.log("currentBetNum: " + this.currentBetNum)
    }

    public onClickRedBtn(btn: cc.Button){
        LogHelp.log("onClickRedBtn: " + btn.node.name)
        let index = this.view.btnNameAry.indexOf(btn.node.name)
        LogHelp.log("onClickRedBtn: " + index)

        if (MtlTopPanelController.Instance.player.haveMoney >= this.currentBetNum ){
            let content = index + "," + this.currentBetNum + "," + false;
            cc.log("打印下注", content)
            GameMtlNetMgr.Instance.sendBet(content)
        }else{
            TipsManager.Instance.createTips("您的钱已经不足，请先充值！")
        }
        
        // let numLab = Utils.FindComponentByName(btn.node, "bet_total_num", cc.Label);
        // let num = Number(numLab.string) + this.currentBetNum
        // numLab.string = "" + num;
    }

    public onClickGreenBtn(btn: cc.Button) {
        LogHelp.log("onClickGreenBtn: " + btn.node.name)
        let index = this.view.btnNameAry.indexOf(btn.node.name)
        LogHelp.log("onClickGreenBtn: " + index)

        if (MtlTopPanelController.Instance.player.haveMoney >= this.currentBetNum) {
            let content = index + "," + this.currentBetNum + "," + false;
            GameMtlNetMgr.Instance.sendBet(content)
        } else {
            TipsManager.Instance.createTips("您的钱已经不足，请先充值！")
        }

        // let numLab = Utils.FindComponentByName(btn.node, "bet_total_num", cc.Label);
        // let num = Number(numLab.string) + this.currentBetNum
        // numLab.string = "" + num;
    }

    public onClickYellowBtn(btn: cc.Button) {
        LogHelp.log("onClickYellowBtn: " + btn.node.name)
        let index = this.view.btnNameAry.indexOf(btn.node.name)
        LogHelp.log("onClickYellowBtn: " + index)

        if (MtlTopPanelController.Instance.player.haveMoney >= this.currentBetNum) {
            let content = index + "," + this.currentBetNum + "," + false;
            GameMtlNetMgr.Instance.sendBet(content)
        } else {
            TipsManager.Instance.createTips("您的钱已经不足，请先充值！")
        }

        // let numLab = Utils.FindComponentByName(btn.node, "bet_total_num", cc.Label);
        // let num = Number(numLab.string) + this.currentBetNum
        // numLab.string = "" + num;
    }

    /**
     * 初始化倍数
     * 倍数(按动物 竖着红，绿，黄)
     * @param oddAndTimes 
     */
    public initOdd(oddAndTimes: Array<string>) {
        this.clearAll()
        this.show()
        // let odd: string[] = oddAndTimes[0].split(",");//倍数

        for (let i = 0; i < oddAndTimes.length; i++) {
            let node = this.view.ansList[i].node
            let amn = Utils.FindComponentByName(node, "an_multiple_num", cc.Label);
            amn.string = oddAndTimes[i];
        }

        this.showBet()
        this.updateBtnStatus(false)
    }

    /**
     * 更新按钮状态
     * @param value 
     */
    public updateBtnStatus(boo: boolean = false) {
        let arr = this.view.ansList
        if (arr != null && arr.length > 0) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (boo == false) {
                    value.node.resumeSystemEvents(true)
                } else {
                    value.node.pauseSystemEvents(true)
                }
            })
        }
    }

    /**
     * 显示筹码
     */
    public showBet() {
        let money = MtlTopPanelController.Instance.player.haveMoney
        if (money >= 0 && money <= 10) {
            this.setChips(0);
        }
        if (money >= 10 && money <= 100) {
            this.setChips(1);
        }
        if (money >= 100 && money <= 500) {
            this.setChips(2);
        }
        if (money >= 500 && money <= 1000) {
            this.setChips(3);
        }
        if (money >= 1000) {
            this.setChips(4);
        }
    }

    setChips(num: number) {
        let arr = this.arrChips[num]

        this.view.betNum[0].string = arr[0]
        this.view.betNum[1].string = arr[1]
        this.view.betNum[2].string = arr[2]
    }

    /**
     * 5,1000,0
     * 按钮的下标，筹码数,金钱类别（0:金币,1:金牛币,2:银牛币）
     * @param arr 
     */
    public updateBetMoney(arr: string[]) {
        if (arr[2] == "0") {
            let _tempMoney = Number(arr[1]);
            let an = this.view.ansList[Number(arr[0])]
            
            let numLab = Utils.FindComponentByName(an.node, "bet_total_num", cc.Label);
            let num = Number(numLab.string) + _tempMoney
            numLab.string = "" + num;
        }
    }

    public show(){
        this.view.Show()
    }

    public hide(){
        this.view.Hide()
    }

    public clearAll() {
        this.reset()
    }

    public reset(){
        this.view.resetData()
    }
    
}
