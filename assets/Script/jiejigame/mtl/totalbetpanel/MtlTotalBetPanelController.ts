import MtlTotalBetPanelView from './MtlTotalBetPanelView';
import LogHelp from '../../../Log/LogHelp';
import TweenMgr from '../../../utils/TweenMgr';
import { Utils } from '../../../utils/Utils';

/**
 * 总下注面板
 */
export default class MtlTotalBetPanelController {
    public static Instance: MtlTotalBetPanelController = null
    private view: MtlTotalBetPanelView

    constructor(v: MtlTotalBetPanelView){
        MtlTotalBetPanelController.Instance = this
        this.view = v

        this.view.btn_switch.node.on('toggle', this.onClickSwitchCB, this);
    }

    private onClickSwitchCB(toggle:cc.Toggle){
        // LogHelp.log("onClickSwitchCB  " + toggle.isChecked)
        let op = this.view.oldPos
        if (toggle.isChecked == false){
            TweenMgr.moveTo(this.view.node, 0.5, (op.x + this.view.node.width), op.y);
        }else{
            TweenMgr.moveTo(this.view.node, 0.5, op.x, op.y);
        }
        
    }

    /**
     * 更新/初始化 总投注面板
     * @param msg 
     */
   public updateColorMoney(msg: string[]) {
        var ary = msg[0].split(",");
        let arr = this.view.ansList

        if (arr != null && arr.length > 0) {
            arr.forEach((value: cc.Sprite, index: number) => {
                let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
                btn.string = ary[index];
            })
        }

        this.view.total_BNum.string = msg[1]//总金额
        // msg[2]//总人数
    }

    public show() {
        this.view.Show()
    }

    public hide() {
        this.view.Hide()
    }

    public clearAll() {
        this.view.resetData()
    }

}
