import { UIBase } from '../../../components/UIBase';
import { Utils } from '../../../utils/Utils';
import MtlTotalBetPanelController from './MtlTotalBetPanelController';
const {ccclass, property} = cc._decorator;

/**
 * 总下注面板
 */
@ccclass
export default class MtlTotalBetPanelView extends UIBase {

    public btn_switch: cc.Toggle;
    public total_BNum: cc.Label;

    public redList: cc.Sprite[];
    public greenList: cc.Sprite[];
    public yellowList: cc.Sprite[];
    public ansList: cc.Sprite[];

    private comp: MtlTotalBetPanelController

    public oldPos: cc.Vec2;

    public Awake(){
        this.Show()
        this.oldPos = this.node.position

        this.btn_switch = this.FindChildByName("btn_switch", cc.Toggle)
        this.total_BNum = this.FindChildByName("total_BNum", cc.Label)

        this.redList = []
        this.greenList = []
        this.yellowList = []
        this.ansList = []

        let red: cc.Node = this.FindNodeByName("red")
        let arr = red.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    this.redList.push(value)
                }
            })

            this.redList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        let green: cc.Node = this.FindNodeByName("green")
        arr = green.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    this.greenList.push(value)
                }
            })

            this.greenList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        let yellow: cc.Node = this.FindNodeByName("yellow")
        arr = yellow.getComponentsInChildren(cc.Sprite);
        if (arr != null) {
            arr.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("an_bg") != -1) {
                    this.yellowList.push(value)
                }
            })

            this.yellowList.sort((n1, n2) => {
                let sortIndex1 = Number(n1.node.name.substring(5));
                let sortIndex2 = Number(n2.node.name.substring(5));
                if (sortIndex1 > sortIndex2) {
                    return 1;
                }
                if (sortIndex1 < sortIndex2) {
                    return -1;
                }
                return 0;
            });
        }

        for (let index = 0; index < 4; index++) {
            this.ansList.push(this.redList[index]);
            this.ansList.push(this.greenList[index]);
            this.ansList.push(this.yellowList[index]);
        }

        for (let index = 0; index < this.ansList.length; index++) {
            this.ansList[index].node.name = "btn" + index
        }

        this.resetData()

        this.comp = new MtlTotalBetPanelController(this)
    }

    public resetData() {
        this.btn_switch.isChecked = true;
        this.total_BNum.string = "0";

        if (this.ansList != null && this.ansList.length > 0){
            this.ansList.forEach((value: cc.Sprite, index: number) => {
                if (value.node.name.indexOf("btn") != -1) {
                    let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
                    btn.string = "0";
                }
            })
        }

        // if (this.greenList != null && this.greenList.length > 0) {
        //     this.greenList.forEach((value: cc.Sprite, index: number) => {
        //         if (value.node.name.indexOf("an_bg") != -1) {
        //             let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
        //             btn.string = "0";
        //         }
        //     })
        // }

        // if (this.yellowList != null && this.yellowList.length > 0) {
        //     this.yellowList.forEach((value: cc.Sprite, index: number) => {
        //         if (value.node.name.indexOf("an_bg") != -1) {
        //             let btn = Utils.FindComponentByName(value.node, "bet_total_num", cc.Label);
        //             btn.string = "0";
        //         }
        //     })
        // }
    }

}
