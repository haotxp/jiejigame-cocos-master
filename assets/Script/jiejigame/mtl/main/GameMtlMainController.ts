import GameMtlMainView from './GameMtlMainView';
import GameMtlNetMgr from '../net/GameMtlNetMgr';
import { Utils } from '../../../utils/Utils';
import { Define, PlayerDirectionType } from '../../../Define/Define';
import LogHelp from '../../../Log/LogHelp';
import HTTP from '../../../net/HTTP';
import SocketEnumMtl from '../net/SocketEnumMtl';
import { Player, ServerConfig } from '../../../manager/UserMgr';
import UserMgr from '../../../manager/UserMgr';
const {ccclass, property} = cc._decorator;

/**
 * 游戏主类 入口
 */
@ccclass
export default class GameMtlMainController extends cc.Component {

    public static Instance: GameMtlMainController = null;
    public view: GameMtlMainView;

    private signinParams: object = null

    // private baseUrl: string = 'http://localhost:8999/';
    private baseUrl: string = 'http://140.143.193.253:8999/';
    // private baseUrl: string = 'https://api.twomahjong.com/';

    public currentPlayer: Player = null;

    public initailView(v: GameMtlMainView){
        GameMtlMainController.Instance = this;
        this.view = v;

        GameMtlNetMgr.Instance.AddRegisterMessage();

        // 进入游戏 login
        this.Awake();
        
    }

    public Awake() {
        // 存储登陆参数
        let sp = Utils.GetLocalStorage(Define.STORAGE_PARAMS)
        // LogHelp.log(sp)
        let username = "blockgames11";
        let txID = "";
        let token = "";
        let msgTypw = SocketEnumMtl.MESSAGE_CONFIG_GET_SERVER_DEV

        if (!Define.isDebug) {
            this.signinParams = JSON.parse(sp);
            username = this.signinParams['account'];
            txID = this.signinParams['txID'];
            token = this.signinParams['token'];
            msgTypw = SocketEnumMtl.MESSAGE_CONFIG_GET_SERVER
        }

        HTTP.Instance.httpPost(
            this.baseUrl + msgTypw,
            { username, txID, token },
            "",
            (resp) => {
                // LogHelp.log(resp);
                var data = JSON.parse(resp);
                if (data.code == HTTP.SUCCESS) {
                    var token = data.data;

                    // 存储token
                    Utils.SetLocalStorage(Define.STORAGE_TOKEN, token);
                    Utils.SetLocalStorage(Define.STORAGE_PARAMS, sp)
                    this.onGameLogin(token);
                } else {
                    if (data != -1) {
                        console.error(data.msg);
                    }
                }
            }
        );
    }

    /**
     * 游戏登陆
     * @param token token
     */
    onGameLogin(token: string) {
        let verNum = "1.0.0"
        let appId = "10001"
        let username = "10001"
        let msgType = ""
        if (Define.isDebug) {
            verNum = "0.0.1"
            appId = "10000"
            username = "blockgames11"
            msgType = SocketEnumMtl.MESSAGE_GAME_LOGIN_DEV
        } else {
            verNum = "1.0.0"
            appId = "10001"
            username = this.signinParams['account']
            msgType = SocketEnumMtl.MESSAGE_GAME_LOGIN_PROD
        }
        var obj = {
            "verNum": verNum,
            "token": token,
            "appId": appId,
            "username": username
        };

        Utils.SetLocalStorage(Define.STORAGE_APP_ID, appId);
        Utils.SetLocalStorage(Define.STORAGE_VER_NUM, verNum);

        HTTP.Instance.httpPost(this.baseUrl + msgType, obj, token, (resp) => {
            var repData = JSON.parse(resp);
            if (repData.code == HTTP.SUCCESS) {
                var returnData = repData.data.data.returnData;
                let player: Player = UserMgr.Instance.getPlayer(PlayerDirectionType.DOWN);
                player.haveMoney = returnData.haveMoney;
                player.nickName = returnData.nickName;
                player.playerId = returnData.playerId;
                player.headUrl = returnData.headUrl;
                player.loginDays = returnData.loginDays;

                player.token = returnData.token;
                player.hallConfig = returnData.hallConfig;
                this.currentPlayer = player;
                UserMgr.Instance.joinPlayer(PlayerDirectionType.DOWN, player);

                // 存储当前用户ID
                Utils.SetLocalStorage(Define.STORAGE_PLAYER_ID, player.playerId);
                // LogHelp.log(player.playerId + "  :  " + player.haveMoney);

                this.onGetServerConfig();
            } else {
                if (repData != -1) {
                    console.error(repData.msg);
                }
            }
        });
    }

    onGetServerConfig() {
        let obj = {
            "gameType": "hoolaiforestdance",
            "token": Utils.GetLocalStorage(Define.STORAGE_TOKEN),
            "appId": Utils.GetLocalStorage(Define.STORAGE_APP_ID),
            "verNum": Utils.GetLocalStorage(Define.STORAGE_VER_NUM)
        };

        HTTP.Instance.httpPost(this.baseUrl + SocketEnumMtl.MESSAGE_GAME_SERVER_CONFIG, obj, Utils.GetLocalStorage(Define.STORAGE_TOKEN), (resp) => {
            // LogHelp.log(resp);
            let data = JSON.parse(resp);
            if (data.code == HTTP.SUCCESS) {
                let room = data.data;

                let conf: ServerConfig = UserMgr.Instance.initailServerConfig(room.room1);
                // LogHelp.log(JSON.stringify(conf));

                Define.MTL__WEB__SOCKET__URL = 'ws://' + conf.servers + '/websocket';

                GameMtlNetMgr.Instance.connecting();
            } else {
                if (data != -1) {
                    console.error(data.msg);
                }
            }
        });
    }

}
