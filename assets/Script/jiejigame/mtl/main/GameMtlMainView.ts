import { UIBase } from '../../../components/UIBase';
import GameMtlTopUILayer from '../GameMtlTopUILayer';
import GameMtlMainController from './GameMtlMainController';
import MtlTopPanelView from '../toppanel/MtlTopPanelView';
import MtlTotalBetPanelView from '../totalbetpanel/MtlTotalBetPanelView';
import MtlBetPanelView from '../betpanel/MtlBetPanelView';
import MtlResultPanelView from '../resultpanel/MtlResultPanelView';
import MtlWinRecordPanelView from '../winrecordpanel/MtlWinRecordPanelView';
import MtlGamePanelView from '../forestgamepanel/MtlGamePanelView';
import MtlGamePanelController from '../forestgamepanel/MtlGamePanelController';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMtlMainView extends UIBase {

    private comp: GameMtlMainController;

    onLoad() {
        this.Awake();
    }

    public Awake() {

        let topLayer: GameMtlTopUILayer = this.FindChildByName("game_top_ui_layer", GameMtlTopUILayer);
        topLayer.Awake();

        let topPanel: MtlTopPanelView = this.FindChildByName("topPanel", MtlTopPanelView);
        topPanel.Awake();

        let totalPanel: MtlTotalBetPanelView = this.FindChildByName("totalBetPanel", MtlTotalBetPanelView);
        totalPanel.Awake();

        let betPanel: MtlBetPanelView = this.FindChildByName("betPanel", MtlBetPanelView);
        betPanel.Awake();

        let rp: MtlResultPanelView = this.FindChildByName("resultPanel", MtlResultPanelView);
        rp.Awake();

        let wrp: MtlWinRecordPanelView = this.FindChildByName("winRecordPanel", MtlWinRecordPanelView);
        wrp.Awake();

        let fgp: MtlGamePanelView = this.FindChildByName("forestGamePanel", MtlGamePanelView);
        fgp.Awake();

        this.comp = new GameMtlMainController();
        this.comp.initailView(this);

        // MtlGamePanelController.Instance.playMtl(['2', '5'])

    }

}
