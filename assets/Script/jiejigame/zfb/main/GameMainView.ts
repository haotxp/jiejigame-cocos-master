import GameMainController from './GameMainController';
import { UIBase } from '../../../components/UIBase';
import GameZfbTopUILayer from '../GameZfbTopUILayer';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMainView extends UIBase {

    public btn_rules: cc.Button;
    public btnCancelTing: cc.Button;
    private comp: GameMainController;

    start() {
        this.Awake();
    }

    public Awake() {

        let topLayer: GameZfbTopUILayer = this.FindChildByName("game_top_ui_layer", GameZfbTopUILayer);
        topLayer.Awake();

        // let ggc: GameGlobalChupai = this.FindChildByName("global_chupai", GameGlobalChupai);
        // ggc.Awake()

        // this.btn_rules = this.FindChildByName("btn_rules", cc.Button);
        // this.btnCancelTing = this.FindChildByName("btnCancelTing", cc.Button);
        // this.btnCancelTing.node.active = false

        this.comp = new GameMainController();
        this.comp.initailView(this);

    }

}
