import { Utils } from '../../../utils/Utils';
import { Define, PlayerDirectionType } from '../../../Define/Define';
import HTTP from '../../../net/HTTP';
import SocketEnumZfb from '../net/SocketEnumZfb';
import LogHelp from '../../../Log/LogHelp';
import { Player, ServerConfig } from '../../../manager/UserMgr';
import UserMgr from '../../../manager/UserMgr';
import GameZfbNetMgr from '../net/GameZfbNetMgr';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMainStart extends cc.Component {

    private static __instance: GameMainStart;
    public static get Instance() {
        if (null == this.__instance) {
            this.__instance = new GameMainStart();
        }
        return this.__instance;
    }

    private signinParams: object = null

    // private signinParams: string = '{"username":"blockgames11","token":"8ecef50ac8defbe3bc641a3b3bb6e6ee","txID":"314375c0694768e3c3690570b3409da58b0beabc24fdb6b66e72f674ff0feb7d"}';

    // private loginParams: string = '{ "verNum": "0.0.1", "txID": "781578bb948b15c09203ffbb604f297fa4a9fd9e2119a53466045e4539262c23", "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJibG9ja2dhbWVzMTEiLCJhdXRoIjoiUk9MRV9DTElFTlQiLCJpYXQiOjE1NTU5MjE5NDcsImV4cCI6MTU2MTEwNTk0N30.26pYyHRz0_rTZlVCE9cIGBHYktE8UyIU8N1ye0SGfBM", "appId": "10000", "username": "blockgames11" }';

    // private baseUrl: string = 'http://localhost:8999/';
    // private baseUrl: string = 'http://140.143.193.253:8999/';
    private baseUrl: string = 'https://api.twomahjong.com/';

    public currentPlayer: Player = null;

    public Awake() {

        // var encrypt=require('./encryptjs.js');
        // var secretkey='ios';
        // var plaintext='apple';
        // var filepath="./encryptedData.txt";
        // var cipherText =encrypt.encrypt(plaintext,secretkey,256);
        // console.log(cipherText+" ****************** ");
        // var decipher=encrypt.decrypt(cipherText,secretkey,256);
        // console.log("Deciphred Text is : "+decipher);

        // 存储登陆参数
        let sp = Utils.GetLocalStorage(Define.STORAGE_PARAMS)
        LogHelp.log(sp)
        this.signinParams = JSON.parse(sp);
        let username = this.signinParams['account'];
        let txID = this.signinParams['txID'];
        let token = this.signinParams['token'];

        HTTP.Instance.httpPost(
            this.baseUrl + SocketEnumZfb.MESSAGE_CONFIG_GET_SERVER,
            { username, txID, token},
            "",
            (resp) => {
                // LogHelp.log(resp);
                var data = JSON.parse(resp);
                if (data.code == HTTP.SUCCESS) {
                    var token = data.data;

                    // 存储token
                    Utils.SetLocalStorage(Define.STORAGE_TOKEN, token);
                    Utils.SetLocalStorage(Define.STORAGE_PARAMS, sp)
                    this.onGameLogin(token);
                } else {
                    if (data != -1) {
                        console.error(data.msg);
                    }
                }
            }
        );
    }

    /**
     * 游戏登陆
     * @param token token
     */
    onGameLogin(token: string){
        // var obj = {
        //     "verNum": "0.0.1",  
        //     "token": token, 
        //     "appId": "10000", 
        //     "username": this.signinParams['account']
        // };

        var obj = {
            "verNum": "1.0.0",
            "token": token,
            "appId": "10001",
            "username": this.signinParams['account']
        };
        
        Utils.SetLocalStorage(Define.STORAGE_APP_ID, '10001');
        Utils.SetLocalStorage(Define.STORAGE_VER_NUM, '1.0.0');

        HTTP.Instance.httpPost(this.baseUrl + SocketEnumZfb.MESSAGE_GAME_LOGIN, obj, token, (resp) => {
            var repData = JSON.parse(resp);
            if(repData.code == HTTP.SUCCESS){
                var returnData = repData.data.data.returnData;
                let player: Player = UserMgr.Instance.getPlayer(PlayerDirectionType.DOWN);
                player.haveMoney = returnData.haveMoney;
                player.nickName = returnData.nickName;
                player.playerId = returnData.playerId;
                player.headUrl = returnData.headUrl;
                player.loginDays = returnData.loginDays;

                player.token = returnData.token;
                player.hallConfig = returnData.hallConfig;
                this.currentPlayer = player;
                UserMgr.Instance.joinPlayer(PlayerDirectionType.DOWN, player);
                
                // 存储当前用户ID
                Utils.SetLocalStorage(Define.STORAGE_PLAYER_ID, player.playerId);
                // LogHelp.log(player.playerId + "  :  " + player.haveMoney);

                this.onGetServerConfig();
            }else{
                if (repData != -1) {
                    console.error(repData.msg);
                }
            }
        });
    }

    onGetServerConfig(){
        let obj = {
            "gameType": "twoPeople",
            "token": Utils.GetLocalStorage(Define.STORAGE_TOKEN),
            "appId": Utils.GetLocalStorage(Define.STORAGE_APP_ID), 
            "verNum": Utils.GetLocalStorage(Define.STORAGE_VER_NUM)
        };

        HTTP.Instance.httpPost(this.baseUrl + SocketEnumZfb.MESSAGE_GAME_SERVER_CONFIG, obj, Utils.GetLocalStorage(Define.STORAGE_TOKEN), (resp) => {
            // LogHelp.log(resp);
            let data = JSON.parse(resp);
            if (data.code == HTTP.SUCCESS){
                let room = data.data;

                let conf : ServerConfig = UserMgr.Instance.initailServerConfig(room.room1);
                // LogHelp.log(JSON.stringify(conf));

                GameZfbNetMgr.Instance.connecting();
            }else{
                if (data != -1){
                    console.error(data.msg);
                }
            }
        });
    }

}
