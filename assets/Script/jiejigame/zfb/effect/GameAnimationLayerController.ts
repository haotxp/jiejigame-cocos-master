import GameAnimationLayerView from './GameAnimationLayerView';
import { PlayerDirectionType, GameAnimationNames } from '../../../Define/Define';
import { GameLoadResourceMgr } from '../../../Load/GameLoadResourceMgr';

export default class GameAnimationLayerController {

    public static Instance: GameAnimationLayerController = null

    public view: GameAnimationLayerView

    public constructor(v: GameAnimationLayerView){
        GameAnimationLayerController.Instance = this
        this.view = v
    }

    public showAnimation(dir: PlayerDirectionType, name: GameAnimationNames){
        let sd = GameLoadResourceMgr.Instance.GetTempLoadResourceControl().getSkeletonDataRes(name)
        if (sd == null){
            return
        }
        let currentSk: sp.Skeleton
        switch (dir) {
            case PlayerDirectionType.CENTER:
                currentSk = this.view.game_effect_center
                break;
            case PlayerDirectionType.DOWN:
                currentSk = this.view.game_effect_down
                break;
            case PlayerDirectionType.RIGHT:

                break;
            case PlayerDirectionType.UP:
                currentSk = this.view.game_effect_up
                break;
            case PlayerDirectionType.LEFT:

                break;
        }

        if (currentSk != null) {
            currentSk.node.active = true
            currentSk.skeletonData = sd
            currentSk.setAnimation(0, name, false)

            currentSk.setCompleteListener(() => {
                currentSk.node.active = false
                currentSk.skeletonData = null
            })
            
        }
    }
    
}
