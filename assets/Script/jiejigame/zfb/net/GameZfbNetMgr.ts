import WebSocketManage from '../../../net/WebSocketManage';
import { GameSocketType, Define } from '../../../Define/Define';
import GameMainStart from '../main/GameStart';
import { Utils } from '../../../utils/Utils';
import SocketEnumZfb from './SocketEnumZfb';
import { NotificationManage } from '../../../Notification/NotificationManage';
import { GameAudioMgr } from '../../../manager/GameAudioMgr';
import AlertPanelManager from '../../../manager/AlertPanelManager';
import LogHelp from '../../../Log/LogHelp';

const {ccclass, property} = cc._decorator;

/**
 * 二麻统一消息处理
 */
@ccclass
export default class GameZfbNetMgr extends cc.Component {

    private static __instance: GameZfbNetMgr;
    public static get Instance() {
        if (null == this.__instance) {
            this.__instance = new GameZfbNetMgr();
        }
        return this.__instance;
    }

    /** 自己的方位 */
    public directionDown: number = -1;
    /** 庄的方位 */
    public directionZhuang: number = -1;

    /**
     * 开始连接socket
     */
    public connecting() {
        WebSocketManage.Instance.CreateWebSocket(GameSocketType.SocketType_Twomahjong,
            Define.TWOMAHJONG__WEB__SOCKET__URL,
            this.onConnectedSuccessCB.bind(this));
    }

    /**
     * 关闭游戏连接
     */
    public closeConnected(){
        WebSocketManage.Instance.CloseWebSocker(GameSocketType.SocketType_Twomahjong);
    }
   
    /**
     * 连接成功
     * @param key 
     */
    public onConnectedSuccessCB(){
        // LogHelp.log("连接成功");
        this.joinDesk()
    }

    public joinDesk(){
        if (WebSocketManage.Instance.isConnecting(GameSocketType.SocketType_Twomahjong) == -1){
            this.connecting()
            return;
        }
        if (WebSocketManage.Instance.isConnecting(GameSocketType.SocketType_Twomahjong) == 0) {
            WebSocketManage.Instance.ReconnectWebSocker(GameSocketType.SocketType_Twomahjong);
            return;
        }
        let json = {
            "playerId": GameMainStart.Instance.currentPlayer.playerId,
            "token": Utils.GetLocalStorage(Define.STORAGE_TOKEN)
        }
        // 连接成功后，发送进入桌子请求
        this.SendMessage(SocketEnumZfb.JOINDESK, JSON.stringify(json));
    }

    /**
     * 统一发送消息
     * 
     * @param command 
     * @param data 
     */
    public SendMessage(command: number, data: string = "") {
        // LogHelp.log(command + '，data: ' + data);
        data = command + "@" + data;

        WebSocketManage.Instance.SendMessage(GameSocketType.SocketType_Twomahjong, command, data);
    }

    //==========================接收消息处理=============

    /**
     * 注册消息
     */
    public AddRegisterMessage() {

        // 初始化桌子
        NotificationManage.Register(SocketEnumZfb.initDeskI,
            this.initDeskS.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 加入桌子
        NotificationManage.Register(SocketEnumZfb.joinDeskI,
            this.joinDeskS.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 退出桌子
        NotificationManage.Register(SocketEnumZfb.exitDeskI,
            this.exitDesk.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 更新玩家状态
        NotificationManage.Register(SocketEnumZfb.updatePlayerStateI,
            this.updatePlayerStatus.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 开始游戏发牌
        NotificationManage.Register(SocketEnumZfb.beginGameI,
            this.beginGame.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 游戏结束
        NotificationManage.Register(SocketEnumZfb.gameOverI,
            this.onGameOver.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 超时等待提示 错误提示
        NotificationManage.Register(SocketEnumZfb.newTishiI,
            this.onTishi.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 显示倒计时
        NotificationManage.Register(SocketEnumZfb.showLimitTimeI,
            this.showLimitTime.bind(this), this, GameSocketType.SocketType_Twomahjong);

        NotificationManage.Register(SocketEnumZfb.unShowLimitTimeI,
            this.unshowLimitTime.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 更新金币
        NotificationManage.Register(SocketEnumZfb.updateMoneyI,
            this.updateMoney.bind(this), this, GameSocketType.SocketType_Twomahjong);

        // 被踢出局
        NotificationManage.Register(SocketEnumZfb.beiTiI,
            this.onBeiTi.bind(this), this, GameSocketType.SocketType_Twomahjong);
    }

    /**
     * 初始化桌子
     * 
     * @param command 
     * @param data 
     */
    private initDeskS(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let dir: number = Number(realSmg[1]); // 自己的方位
            this.directionDown = dir;

        }
    }

    /**
     * 加入桌子
     * 
     * @param command 
     * @param data 
     */
    private joinDeskS(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            //GameAudioMgr.Instance.PlaySoundCommon("joinDesk.mp3"); //暂无资源，先关闭
            let joinAry: Array<string> = realSmg[1].split(','); // 显示玩家个人信息
            let dir: number = Number(joinAry[0]);
            // if (this.directionDown == dir) {
            //     MahjongManager.Instance.posDown = dir;
            //     GamePlayerDownController.Instance.joinDesk(joinAry);
            // } else {
            //     GamePlayerUpController.Instance.joinDesk(joinAry);
            // }
        }
    }

    /**
     * 退出桌子
     * 
     * @param command 
     * @param data 
     */
    private exitDesk(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let joinAry: Array<string> = realSmg[1].split(','); // 显示玩家个人信息
            let dir: number = Number(joinAry[0]);

        }
    }

    /**
     * 更新玩家状态
     * 
     * @param command 
     * @param data 
     */
    private updatePlayerStatus(command: string, data: string) {
        // LogHelp.log(command + '，data: ' + data);
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let stateAry: Array<string> = realSmg[1].split(',');

            let dir: number = Number(stateAry[0]);
            let status: string = stateAry[1];

        }
    }

    /**
     * 开始游戏发牌
     * 
     * @param command 
     * @param data 
     */
    private beginGame(command: string, data: string) {
        // LogHelp.log(command + '，data: ' + data);
        let realSmg = data.split('@');
        if (realSmg.length > 0) {


        }
    }

    /**
     * 游戏结束
     * 
     * @param command 
     * @param data 
     */
    private onGameOver(command: string, data: string) {
        // this.isPlayPoker = false;
        // GameAnimationLayerController.Instance.showAnimation(PlayerDirectionType.CENTER, GameAnimationNames.END)
        // let realSmg = data.split('@');
        // if (realSmg.length > 0) {
        //     let allInfo: Array<string> = realSmg[1].split(';');
        //     MahjongManager.Instance.gameOver(allInfo)
        //     GameResultPanelController.Instance.showResult(realSmg[1])
        // }

        // MahjongManager.Instance.isClickTing = -1
        // GameTuoguanPanelController.Instance.show(false)
        // GameHuPaiTipsController.Instance.clearObjects()
        // GameOptionPanelController.Instance.reset()
    }

    /**
     * 超时等待提示 错误提示
     * 
     * @param command 
     * @param data 
     */
    private onTishi(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let tishiAry: Array<string> = realSmg[1].split(';');
            if (tishiAry.length > 1) {
                var strT: string = tishiAry[1];
                var tipsType: number = -1;

                if (tishiAry.length >= 3) {
                    tipsType = Number(tishiAry[2]);
                }

                if (tishiAry[3] == null || tishiAry[3] == "") {
                    if (tipsType == SocketEnumZfb.Tishi_notEnoughMonney) {
                        // LogHelp.log("点击返回大厅0")
                        AlertPanelManager.Instance.show(strT, function () {
                            // CommonConnInterface.instance.onRechargeEos();
                        }, function () {
                            // CommonConnInterface.instance.onRechargeEos();
                        });

                    } else {
                        // 返回大厅
                        AlertPanelManager.Instance.show(strT, () => {
                            // LogHelp.log("点击返回大厅1")
                            this.joinDesk()
                            //点击返回大厅
                        }, () => {
                            //点击返回大厅
                        });
                    }
                }
                else {
                    //等待超时弹出面板方法
                    if (tipsType == SocketEnumZfb.Tishi_returnHall) {
                        AlertPanelManager.Instance.show(strT, () => {
                            // LogHelp.log("点击返回大厅2")
                            this.joinDesk()
                            //点击返回大厅
                        }, () => {
                            //点击返回大厅
                        });
                    } else {
                        AlertPanelManager.Instance.show(strT, () => {
                            // LogHelp.log("点击返回大厅2")
                            //点击返回大厅
                        }, () => {
                            //点击返回大厅
                        });
                    }

                }
            } else {
                // TipsManager.instance.showTipsMiddle(new MiddleTips(), tishiAry[0].toString());
            }
        }
    }

    /**
     * 显示倒计时
     * 
     * @param command 
     * @param data 
     */
    private showLimitTime(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let count: number = Number(realSmg[1]);
        }
    }

    private unshowLimitTime(command: string, data: string) {
    }

    /**
     * 更新金币
     * 
     * @param command 
     * @param data 
     */
    private updateMoney(command: string, data: string) {
        let realSmg = data.split('@');
        if (realSmg.length > 0) {
            let arr: Array<string> = realSmg[1].split(',');
            let myUpMoney = Number(arr[this.directionDown]);

            // GamePlayerDownController.Instance.updateHaveMoneyView(myUpMoney);
            // GamePlayerUpController.Instance.updateHaveMoney(arr);
        }
    }

    /**
     * 被踢出局
     * 
     * @param command 
     * @param data 
     */
    private onBeiTi(command: string, data: string) {
        // LogHelp.log("被踢出局")
        // GamePlayerUpController.Instance.exitDesk();
        // GameTwomahjongNetMgr.Instance.closeConnected();

        // GamePlayerDownController.Instance.showReady(-1);
    }

    //========================================


}
