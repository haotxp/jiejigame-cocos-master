
const {ccclass, property} = cc._decorator;

/**
 * 消息号
 */
@ccclass
export default class SocketEnumZfb extends cc.Component {
    //======================http============================

    //登陆获取token
    public static readonly MESSAGE_CONFIG_GET_SERVER: string =
        "gameLogin/signin";
    //游戏登陆
    public static readonly MESSAGE_GAME_LOGIN: string = "gameHall/onLoginDev";
    // public static readonly MESSAGE_GAME_LOGIN: string = "gameHall/onLogin";
    //获取大厅配置文件
    public static readonly MESSAGE_GAME_HALL_CONFIG: string =
        "gameHall/getHallConfig";
    //获取服务器配置文件
    public static readonly MESSAGE_GAME_SERVER_CONFIG: string =
        "gameHall/getServerConfig";
    //获取用户余额
    public static readonly MESSAGE_GAME_PLAYER_BALANCE: string =
        "gameHall/getBalance";

    //=======================c to s===========================

    public static readonly CHANGEDESK: number = -1;
    public static readonly JOINDESK: number = 1;
    public static readonly EXITDESK: number = 2;
    public static readonly UPDATEPLAYERSTATE: number = 3;//准备

    public static readonly PUTONEMAHJONG: number = 4;

    public static readonly CHI: number = 5;
    public static readonly PENG: number = 6;
    public static readonly GANG: number = 7;
    public static readonly HU: number = 8;
    public static readonly CONTINUEGAME: number = 9;
    public static readonly CHAT: number = 10;
    public static readonly TUOGUAN: number = 11;
    public static readonly UNTUOGUAN: number = 12;
    public static readonly GUO: number = 13;
    public static readonly TING: number = 14;

    public static readonly PROFITRECORD: number = -11; //个人信息中的盈利记录
    public static readonly GIVEGIFT: number = -12; //个人信息中的礼物房

    //------------------------------------ s to c===============
    public static readonly newTishiI: number = -1;
    public static readonly initDeskI: number = 1;
    public static readonly joinDeskI: number = 2;
    public static readonly exitDeskI: number = 3;
    public static readonly updatePlayerStateI: number = 4;

    public static readonly beginGameI: number = 5;
    public static readonly showLimitTimeI: number = 6;
    public static readonly unShowLimitTimeI :number = 7;

    public static readonly fapaiwanbiI: number = 8;
    public static readonly buhuaI: number = 9;
    public static readonly getOneMahjongI: number = 10;
    public static readonly putOneMahjongI: number = 11;
    public static readonly showOperationI: number = 12;

    public static readonly pengI: number = 13;
    public static readonly chiI: number = 14;
    public static readonly dianGangI: number = 15;
    public static readonly anGangI: number = 16;
    public static readonly wanGangI: number = 17;
    public static readonly huI: number = 18;
    public static readonly zihuI: number = 19;

    public static readonly tuoguanI: number = 20;
    public static readonly untuoguanI: number = 21;
    public static readonly gameOverI: number = 22;

    public static readonly chatI: number = 23;
    public static readonly startPutMahjongI: number = 24;
    public static readonly tingI: number = 25;
    //		public static readonly getSupplyMoneyI:number=-10;//输光补助
    //		public static readonly profitRecordI:number=-11;//个人信息中的盈利记录
    //		public static readonly giveGiftI:number=-12;//个人信息中的礼物房
    //		public static readonly updatePlayerInfoAfterGiveGiftI:number =-30;//送完礼物后更新玩家信息

    public static readonly updateMoneyI: number = 26;
    public static readonly levelUpI: number = 27;
    public static readonly beiTiI: number = 28;

    //		public static readonly Tishi_wozhidaola:number  = 0;
    public static readonly Tishi_returnHall :number = 1;
    //		public static readonly Tishi_cancel :number = 2;
    //		public static readonly Tishi_reconn :number = 3;
    //		public static readonly Tishi_gotoNextMatch :number = 4;
    //		public static readonly Tishi_exitMatch :number = 5;
    //		public static readonly Tishi_changeDesk :number = 6;
    //		public static readonly Tishi_continueGame:number =7;
    public static readonly Tishi_notEnoughMonney: number = 8;

    // onLoad () {}

    start() {}
}
