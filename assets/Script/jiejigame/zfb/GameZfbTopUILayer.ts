import { UIBase } from '../../components/UIBase';
const {ccclass, property} = cc._decorator;

@ccclass
export default class GameZfbTopUILayer extends UIBase {

    public static Instance: GameZfbTopUILayer

    private currentPanel: cc.Node

    public Awake(){
        this.Hide()
        GameZfbTopUILayer.Instance = this
    }

    public onShowPanel(c: cc.Node){
        if (this.node.isChildOf(c)) return
        this.currentPanel = c
        this.Show()
        this.node.addChild(c)
    }

    public Hide(){
        super.Hide()
        this.node.active = false
        this.node.removeChild(this.currentPanel)
    }


}
