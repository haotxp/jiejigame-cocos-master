
import UserMgr from "../manager/UserMgr";
import HTTP from "../net/HTTP";
import SceneManager from "../manager/SceneManager";
import WebSocketManage from "../Net/WebSocketManage";
import LogHelp from '../Log/LogHelp';
import { GameAudioMgr } from '../manager/GameAudioMgr';
import { UIManager } from '../manager/UIManager';
import { GameLoadResourceMgr } from '../Load/GameLoadResourceMgr';
import { Define, GameSceneName, GameType } from '../Define/Define';
import { Utils } from '../utils/Utils';
import { Base64 } from 'js-base64';
import TipsManager from '../manager/TipsManager';
import AlertPanelManager from '../manager/AlertPanelManager';

const { ccclass, property } = cc._decorator;

@ccclass
export default class AppStart extends cc.Component {

    private static Instace: AppStart;

    private progress_label: cc.Label = null;
    private splash: cc.Node = null;

    private urlParams = {}

    onLoad() {
        AppStart.Instace = this

        this.progress_label = Utils.FindComponentByName(this.node, "progress_label", cc.Label)
        this.splash = Utils.FindChildNodeByName(this.node, "splash")

        this.progress_label.string = "正在拼命加载中…………";

        if(!cc.sys.isNative && cc.sys.isMobile){
            var canvas = this.node.getComponent(cc.Canvas);
            canvas.fitHeight = true;
            canvas.fitWidth = true;
        }
        let win = cc.winSize; //cc.director.getWinSize() ;
        cc.view.setDesignResolutionSize(win.width, win.height, cc.ResolutionPolicy.EXACT_FIT);
        
        this.initManager();
        
        GameAudioMgr.Instance.PlaySoundBG("bgsound.mp3", true);

        this.urlParams = this.urlParse()
        console.log("读取页面参数： " + this.urlParams)

        if (this.urlParams != null && this.urlParams['data'] != null){
            Utils.SetLocalStorage(Define.STORAGE_PARAMS, Base64.decode(this.urlParams['data']) )
        }

        // 设置调试状态
        Define.isDebug = true
        // Define.isDebug = false
        if (Define.isDebug == true) {
            LogHelp.OPENLOGFLAG = true
        }else{
            LogHelp.OPENLOGFLAG = false
        }

        //===========当前进了哪个游戏============
        Define.currentGameType = GameType.Game_MTL
        //Define.currentGameType = GameType.Game_ZFB
        let resUrl = ""
        let scenceUrl = ""

        if (Define.currentGameType == GameType.Game_MTL){
            resUrl = Define.__MTL__PRELOADING__PATH
            scenceUrl = GameSceneName.SCENE_MTL
        } else if (Define.currentGameType == GameType.Game_ZFB) {
            resUrl = Define.__ZFB__PRELOADING__PATH
            scenceUrl = GameSceneName.SCENE_ZFB
        }
        
        //=======================
        // 检查更新
        this.showSplash(function () {
            // this.getServerInfo();
            this.progress_label.string = "正在拼命加载中" + 0 + "%…………";
            GameLoadResourceMgr.Instance.GetTempLoadResourceControl().LoadTempResource(resUrl,
                () => {
                    SceneManager.Instance.switchScene(scenceUrl, () => {
                        // UIManager.Instance.Initial();
                        LogHelp.log("加载完成")
                        // GameAudioMgr.Instance.PlaySoundBG("bgsound.mp3", true);
                        // TipsManager.Instance.createTips("加载完成")
                    });
                }, (num: number) => {
                    let n = Number(num.toFixed()) * 100
                    // LogHelp.log("正在拼命加载中…………" + n + "%…………")
                    this.progress_label.string = "正在拼命加载中" + n + "%…………";
                });
        }.bind(this));
    }

    showSplash(callback: Function) {
        let self = this;
        let SHOW_TIME = 3000;
        let FADE_TIME = 500;

        if (true || cc.sys.os != cc.sys.OS_IOS || !cc.sys.isNative) {
            this.splash.active = true;
            if (this.splash.getComponent(cc.Sprite).spriteFrame == null) {
                callback();
                return;
            }
            let t = Date.now();
            let fn = function () {
                let dt = Date.now() - t;
                if (dt < SHOW_TIME) {
                    setTimeout(fn, 33);
                }
                else {
                    let op = (1 - ((dt - SHOW_TIME) / FADE_TIME)) * 255;
                    if (op < 0) {
                        self.splash.opacity = 0;
                        callback();
                    }
                    else {
                        self.splash.opacity = op;
                        setTimeout(fn, 33);
                    }
                }
            };
            setTimeout(fn, 33);
        }
        else {
            this.splash.active = false;
            callback();
        }
    }

    private initManager() {
        LogHelp.log("初始化");

        // 初始化websock
        WebSocketManage.Instance.Initial();
        // 用户管理
        UserMgr.Instance.Initial();
        // 初始化http
        HTTP.Instance.Initial();
        // 初始化场景
        SceneManager.Instance.Initial();
        // 初始化音频
        GameAudioMgr.Instance.Initial();

        TipsManager.Instance.Initial();

        AlertPanelManager.Instance.Initial();
    }

    urlParse() {
        let params = {};
        if (window.location == null) {
            return params;
        }
        let name, value;
        let str = window.location.href; //取得整个地址栏
        let num = str.indexOf("?")
        str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]

        let arr = str.split("&"); //各个参数放到数组里
        for (let i = 0; i < arr.length; i++) {
            num = arr[i].indexOf("=");
            if (num > 0) {
                name = arr[i].substring(0, num);
                value = arr[i].substr(num + 1);
                params[name] = value;
            }
        }
        return params;
    }

    // getServerInfo() {
    //     var self = this;
    //     var onGetVersion = function (ret) {
    //         cc.vv.SI = ret;
    //         if (cc.sys.isNative) {
    //             var url = cc.url.raw('resources/ver/cv.txt');
    //             cc.loader.load(url, function (err, data) {
    //                 cc.VERSION = data;
    //                 if (ret.version == null) {
    //                     console.log("error.");
    //                 }
    //                 else {
    //                     if (cc.vv.SI.version != cc.VERSION) {
    //                         cc.find("Canvas/alert").active = true;
    //                     }
    //                     else {
    //                         cc.director.loadScene(self._mainScene);
    //                     }
    //                 }
    //             }.bind(this));
    //         }
    //         else {
    //             cc.director.loadScene(self._mainScene);
    //         }
    //     };

    //     var xhr = null;
    //     var complete = false;
    //     var fnRequest = function () {
    //         self.loadingProgess.string = "正在连接服务器";
    //         xhr = cc.vv.http.sendRequest("/get_serverinfo", null, function (ret) {
    //             xhr = null;
    //             complete = true;
    //             onGetVersion(ret);
    //         });
    //         setTimeout(fn, 5000);
    //     }

    //     var fn = function () {
    //         if (!complete) {
    //             if (xhr) {
    //                 xhr.abort();
    //                 self.loadingProgess.string = "连接失败，即将重试";
    //                 setTimeout(function () {
    //                     fnRequest();
    //                 }, 5000);
    //             }
    //             else {
    //                 fnRequest();
    //             }
    //         }
    //     };
    //     fn();
    // }

}
