/**
 * 玩家方位 前端显示方位
 */
export enum PlayerDirectionType {
    CENTER = -1,   //中心
    DOWN = 0,   //下方，自己
    RIGHT = 1,  //右边
    UP = 2,     //上方
    LEFT = 3,   //左边
}

/**
 * 玩家game状态
 */
export enum GamePlayerStatus {
    IDLE = "idle",  
    WAIT = "wait",    // 等待中
    GAMING = "gameing"     // 游戏中
}

/**
 * 动画名称
 */
export enum GameAnimationNames {
    BEGIN = "begin",    // 开始
    END = "gameend",    // 结束游戏
    CHI = "chi",     // 吃
    GANG = "gang",     // 杠
    HU = "hu",     // 胡
    PENG = "peng",     // 碰
    TING = "ting"    // 听
}

/**
 * 游戏缓冲池名字
 */
export enum GamePoolsName {
    OPTION_MAHJONG = "option_mahjongs",   // 吃 听 麻将
    OPTION_MAHJONG_TIPS = "option_mahjongs_tips",   // 听麻将低版
    POOL_MAHJONG_PGC_DOWN = "mahjong_pool_pgc_down",   // 麻将碰杠吃
    POOL_MAHJONG_HU_DOWN = "mahjong_pool_hu_down",   // 麻将胡
    POOL_MAHJONG_PGC_UP = "mahjong_pool_pgc_up",   // 麻将碰杠吃
    POOL_MAHJONG_HU_UP = "mahjong_pool_hu_up",   // 麻将胡

    POOL_MTL_ANI = "mtl_pool_ani",   // 摩天轮动物
    POOL_MTL_COLOR = "mtl_pool_color",   // 摩天轮颜色
    POOL_MTL_RECORD_ITEM = "mtl_pool_record_item",   // 摩天轮开奖记录item
}

export enum GameSceneName {
    SCENE_TWOMAHJONG = "twomahjong_game_scene",
    SCENE_CORRIDA = "corrida_game",
    SCENE_ZFB = "game_scene_zfb",
    SCENE_MTL = "game_scene_mtl",
}

/**
 * 游戏 Socket 类型
 */
export enum GameSocketType {
    /** 大厅 */
    SocketType_Hall = 'game_socket_type_hall',
    /** 二人麻将 */
    SocketType_Twomahjong = 'game_socket_type_twomahjong',
    /** 四川麻将 */
    SocketType_SCMahjong = 'game_socket_type_scmahjong',
    /** 斗牛 */
    SocketType_CorridaGame = 'game_socket_type_corrida',
    /** 中发白 */
    SocketType_ZFB = 'game_socket_type_zfb',
    /** 摩天轮 */
    SocketType_MTL = 'game_socket_type_mtl',
    /** 跑马 */
    SocketType_Horse = 'game_socket_type_horse'
}

export enum GameType {
    Game_NONE = -1,
    Game_ZFB = 0,  //中发白
    Game_MTL = 1,  //摩天轮
    Game_DZPK = 2,  //德州扑克
    Game_PPT = 3,  //跑跑堂
    Game_SGJ = 4,  //水果机
    Game_TWOMAHJONG = 5,  //二人麻将
    Game_SCMAHJONG = 6,  //四川麻将
    Game_CARRIDA = 7,  //斗牛
}

export enum eGameLevel {
    Level_1 = 0,  //初级场
    Level_2 = 1,  //中级场
    Level_3 = 2,  //高级场
}

export enum SoundType {
    BG, //背景音乐
    EFFECT, //音效
}

export class Define {

    /**
     * 当前是哪个游戏
     */
    public static currentGameType: GameType = -1;
    /** 是否为调试 */
    public static isDebug: boolean = true;

    /**
    * 本地存储常量Storage
    */
    public static readonly STORAGE_PARAMS = 'params';//初始化登陆参数
    public static readonly STORAGE_TOKEN = 'token';
    public static readonly STORAGE_PLAYER_ID = 'player_id';
    public static readonly STORAGE_APP_ID = 'app_id';
    public static readonly STORAGE_VER_NUM = 'ver_num';

    //二人麻将websocket地址
    public static TWOMAHJONG__WEB__SOCKET__URL = "ws://140.143.193.253:5401/websocket";
    public static MTL__WEB__SOCKET__URL = "ws://140.143.193.253:5401/websocket";

    //预加载资源路径
    public static __PRELOADING__PATH = "Preloading/";
    public static __PRELOADING__PATH1 = "Atlas/";
    public static __AUDIO__PATH = "/sounds/";
    // 二人麻将
    public static Mahjong__PRELOADING__PATH = "MahjongPreloading/";
    public static Corrida__PRELOADING__PATH = "CorridaPreloading/";
    
    //大厅预加载资源路径
    public static __HALL__PRELOADING__PATH = "HallPreloading/";
    //摩天轮资源
    public static __MTL__PRELOADING__PATH = "MTLPreloading/";
    //中发白资源
    public static __ZFB__PRELOADING__PATH = "ZFBPreloading/";

    /**
     * 自己方位对应的显示表 4人麻将
     */
    private static readonly deskAzimuthTable: { [index: number]: { [index: number]: number } } = {
        0: {0: 0, 1: 1, 2: 2, 3: 3 }, 
        1: { 1: 0, 2: 1, 3: 2, 0: 3 }, 
        2: { 2: 0, 3: 1, 0: 2, 1: 3 }, 
        3: { 3: 0, 0: 1, 1: 2, 2: 3 }
    };
    /**
     * 自己方位对应的显示表 2人麻将
     */
    private static readonly deskAzimuthTableTwomj: {[index:number]: {[index:number]: number}} = {
        0: { 0: PlayerDirectionType.DOWN, 1: PlayerDirectionType.UP },
        1: { 1: PlayerDirectionType.DOWN, 0: PlayerDirectionType.UP }
    };

    /**
         * 根据自己和玩家的实际方位获取显示方位
         * @param myAzimuth            自己实际方位
         * @param otherAzimuth        他人实际方位
         * @return                    他人显示方位
         * 自己的显示坐标永远为0方位
         * 所以算法为 大于自己的减自己 小于自己的加自己
         */
    public static getPlayerAzimuthViewByAzimuth(myAzimuth: number, otherAzimuth: number) {
        return Define.deskAzimuthTable[myAzimuth][otherAzimuth];
    }

    /**
     * 根据自己和玩家的实际方位获取显示方位
     * @param myAzimuth            自己实际方位
     * @param otherAzimuth        他人实际方位
     * @return                    他人显示方位
     * 自己的显示坐标永远为0方位
     * 所以算法为 大于自己的减自己 小于自己的加自己
     */
    public static getPlayerAzimuthViewByAzimuthTwomj(myAzimuth: number, otherAzimuth: number) {
        return Define.deskAzimuthTableTwomj[myAzimuth][otherAzimuth];
    }

    /**
     * 获取声音文件名
     * @param n 
     */
    public static getSoundName(n: any): string {
        let result: string = "";

        switch (n) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                result = n+"wan.mp3";
                break;
            case 11:
                result = "dongfeng.mp3";
                break;
            case 12:
                result = "nanfeng.mp3";
                break;
            case 13:
                result = "xifeng.mp3";
                break;
            case 14:
                result = "beifeng.mp3";
                break;
            case 21:
                result = "zhong.mp3";
                break;
            case 22:
                result = "fa.mp3";
                break;
            case 23:
                result = "bai.mp3";
                break;
            case "buhua":
                result = "buhua.mp3";
                break;
            case "chi":
                result = "chi.mp3";
                break;
            case "peng":
                result = "peng.mp3";
                break;
            case "angang":
                result = "gang.mp3";
                break;
            case "diangang":
                result = "gang.mp3";
                break;
            case "wangang":
                result = "gang.mp3";
                break;
            case "ting":
                result = "ting.mp3";
                break;
            case "hu":
                result = "hu.mp3";
                break;
            case "zimo":
                result = "hu.mp3";
                break;
            case "gangshanghua":
                break;
            case "gangshangpao":
                break;
            case "qianggang":
                break;
        }

        return result;
    }

    /**
     * 获取花色资源名字
     * @param n 
     */
    public static getSpriteName(n: number): string {
        let result: string = "";
        switch (n) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                result = "newmahjong2d_game_mahjongs_hs_w0" + n;
                break;
            case 11:
                result = "newmahjong2d_game_mahjongs_hs_east";
                break;
            case 12:
                result = "newmahjong2d_game_mahjongs_hs_south";
                break;
            case 13:
                result = "newmahjong2d_game_mahjongs_hs_west";
                break;
            case 14:
                result = "newmahjong2d_game_mahjongs_hs_north";
                break;
            case 21:
                result = "newmahjong2d_game_mahjongs_hs_z";
                break;
            case 22:
                result = "newmahjong2d_game_mahjongs_hs_f";
                break;
            case 23:
                result = "newmahjong2d_game_mahjongs_hs_b";
                break;
            case 31:
                result = "newmahjong2d_game_mahjongs_hs_chun";
                break;
            case 32:
                result = "newmahjong2d_game_mahjongs_hs_xia";
                break;
            case 33:
                result = "newmahjong2d_game_mahjongs_hs_qiu";
                break;
            case 34:
                result = "newmahjong2d_game_mahjongs_hs_dong";
                break;
            case 35:
                result = "newmahjong2d_game_mahjongs_hs_mei";
                break;
            case 36:
                result = "newmahjong2d_game_mahjongs_hs_lan";
                break;
            case 37:
                result = "newmahjong2d_game_mahjongs_hs_zhu";
                break;
            case 38:
                result = "newmahjong2d_game_mahjongs_hs_ju";
                break;
        }
        return result;
    }
}


/**
 * 麻将当前类型
 */
export enum PokerType {
    /**手牌*/
    ShouPai = 1,
    /**手牌扣牌*/
    ShouPai_KouPai = 2,
    /**碰杠牌*/
    PengGangPai = 3,
    /**出牌*/
    OutPai = 4,
    /**胡牌*/
    HuPai = 5,
    /**倒牌*/
    DaoPai = 6,
}

/**
 * 麻将值
 */
export enum PokerNumber {
    /**一万*/
    W1 = 1,
    /**二万*/
    W2 = 2,
    /**三万*/
    W3 = 3,
    /**四万*/
    W4 = 4,
    /**五万*/
    W5 = 5,
    /**六万*/
    W6 = 6,
    /**七万*/
    W7 = 7,
    /**八万*/
    W8 = 8,
    /**九万*/
    W9 = 9,
    /**东*/
    EAST = 11,
    /**南*/
    SOUTH = 12,
    /**西*/
    WESTERN = 13,
    /**北*/
    NORTH = 14,
    /**中*/
    CENTRE = 21,
    /**发*/
    SEND = 22,
    /**白*/
    WHITE = 23,
    /**春*/
    SPRING = 31,
    /**夏*/
    SUMMER = 32,
    /**秋*/
    AUTUMN = 33,
    /**冬*/
    WINTER = 34,
    /**梅*/
    PLUM = 35,
    /**兰*/
    ORCHID = 36,
    /**竹*/
    BAMBOO = 37,
    /**菊*/
    CHRYSANTHEMUM = 38
}
